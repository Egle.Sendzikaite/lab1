package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.swing.text.AbstractDocument.BranchElement;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        int roundCounter = 1;
        int humanScore = 0;
        int computerScore = 0;

        while (true){
            System.out.println("Let's play round " + roundCounter);
            String playerMove = playerMove();
            //String playerMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            //while (true){
            
            //if(!playerMove.equals("rock") && !playerMove.equals("paper") && !playerMove.equals("scissors"))
            //{System.out.println("I do not understand " + playerMove + "." + " Could you try again?");}
            //else {break;}

            //}
            String computerMove = rpsChoices.get(new Random().nextInt(rpsChoices.size()));

            String result = "Human chose " + playerMove + ", computer chose " + computerMove + ".";

            if (playerMove.equals(computerMove)) {
                System.out.println(result + " It's a tie!");

            } else if(playerMove.equals("rock")) {
                if (computerMove.equals("paper")) {System.out.println(result + " Computer wins!");computerScore ++; }
                else {System.out.println(result + " Human wins!");humanScore ++;}

            } else if (playerMove.equals("paper")){
                if (computerMove.equals("scissors")) {System.out.println(result + " Computer wins!");computerScore ++;}
                else {System.out.println(result + " Human wins!");humanScore ++;}

            } else if (playerMove.equals("scissors")){
                if (computerMove.equals("rock")) {System.out.println(result + " Computer wins!");computerScore ++;}
                else {System.out.println(result + " Human wins!");humanScore ++;}
            }
            
                System.out.println("Score: human " + humanScore + "," + " computer " + computerScore);
            
            
            System.out.println("Do you wish to continue playing? (y/n)?");
            String play = sc.next();
            //System.out.println(play);
            
            if (play.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            } else {roundCounter ++;}
        }   
    }

    public String playerMove() {
        
        while (true) {
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String move = sc.next();
            if(!move.equals("rock") && !move.equals("paper") && !move.equals("scissors"))
            {System.out.println("I do not understand " + move + "." + " Could you try again?");}
            else {return move.toLowerCase();}
        
        }

    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
